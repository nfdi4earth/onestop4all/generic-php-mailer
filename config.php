<?php
    
    define('SMTP_DEBUG', 1);
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages

    define('SMTP_SERVER', );
    // e.g.: define('SMTP_SERVER', 'msx.tu-dresden.de');
    define('SMTP_USER', );
    // e.g.: define('SMTP_USER', 'my_login');
    define('SMTP_PASSWORD', );
    // e.g.: define('SMTP_USER', 'my_password');
