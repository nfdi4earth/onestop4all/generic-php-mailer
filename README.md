# PHPMailer 4 NFDI4Earth

This php-script serves as a backend to provide a mailing functionality to the the User Support contact form in the OneStop4All frontend.

## Functionality

The script is called via HTTP POST or GET with the relevant input data as payload. Those are validated and send via SMTP using [PHPMailer](https://github.com/PHPMailer/PHPMailer).
All required data is pre-defined by input-name to enable a validation check. Changes to the number or naming of the frontend input data stipulates an adjustment of this validation.

## Configuration

To enable the data validation, please use the following values as input-name attributes only:

```bash
name
email
subject
message
```

All relevant SMTP settings can (resp. have to) be configured using the [config.php](config.php) file.

## Request

An exemplary HTTP GET request looks as follows:

`https://usn_mailing.onestop4all.test.n4e.geo.tu-dresden.de/?name=John Doe&email=john.doe@johndoehub.com&subject=John does a test&message=Hi, I'm testing your phpmailer instance`

## Setup

### ...in [NGINX](https://www.nginx.com)

This is an exemplary nginx configuration to maintain the phpmailer:

```config
server {
    listen 443 ssl;

    server_name {{ _domain }};

    ssl_certificate /etc/letsencrypt/live/wildcard.{{ domain }}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/wildcard.{{ domain }}/privkey.pem;

    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

    root {{ usn.phpmailer.path }};

    index index.php;

    add_header 'Access-Control-Allow-Origin' '*';

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.4-fpm.sock;
    }

    # ***ERRORS***
    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
}

```

Current templated version, can be seen in the Ansible templates: [Server Administration](https://git.rwth-aachen.de/nfdi4earth/architecture/server-administration/-/tree/master/roles/onestop4all)

This very helpfull tiny How to helped a lot by building the setup: [How to setup PHP on Nginx with fastCGI (PHP-FPM) example](https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/Nginx-PHP-FPM-config-example)

# License

This project is published under the Apache License 2.0, see file [LICENSE](LICENSE).

Contributors: Christoph Wagner, Markus Konkol, Ralf Klammer
